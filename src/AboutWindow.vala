/*
	Copyright (C) 2015-2025. Vinari Software.
	All rights reserved.​


	Redistribution and use in source and binary forms, source code with or without
	modification, are permitted provided that the following conditions are met:

	1. Redistributions of source code must retain the above copyright
	notice, this list of conditions and the following disclaimer.

	2. Redistributions in binary form must reproduce the above copyright
	notice, this list of conditions and the following disclaimer in the
	documentation and/or other materials provided with the distribution.

	3. Neither the name of the copyright holder nor the names of its
	contributors may be used to endorse or promote products derived
	from this software without specific prior written permission.

	THIS SOFTWARE IS PROVIDED BY VINARI SOFTWARE "AS IS" AND ANY
	EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL VINARI SOFTWARE BE LIABLE FOR ANY
	DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
	LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


	VINARI SOFTWARE 2015-2025
*/


namespace UI {
	public class AboutWindow{

		private Gtk.AboutDialog mainInstance;				//Since inheritence has been disabled, this is the main instance of the Gtk.AboutDialog

		public AboutWindow(Gtk.Window parent){
			mainInstance=new Gtk.AboutDialog();
			mainInstance.set_license_type(Gtk.License.BSD_3);
			mainInstance.set_copyright("Copyright 2015 - 2025 Vinari Software");
			mainInstance.set_website("https://gitlab.com/vinarisoftware/vinari-os-tweaks-gtk");
			mainInstance.set_website_label("Vinari Software");
			mainInstance.set_version("3.0.0 [HELIO-GTK 4]");
			mainInstance.set_program_name("Vinari OS Tweaks - Vinari Software");
			mainInstance.set_logo_icon_name("gnome-tweaks");
			mainInstance.set_comments(_("Vinari OS Tweaks - Vinari Software is a simple utility designed to help the user customize the OS appearance, and user experience to their liking."));

			mainInstance.set_artists(
				{"Vinari Software <vinarisoftware@proton.me>"}
			);
			
			mainInstance.set_authors(
				{"Vinari Software <vinarisoftware@proton.me>"}
			);
			
			mainInstance.set_documenters(
				{"Vinari Software <vinarisoftware@proton.me>"}
			);
			
			mainInstance.set_translator_credits("Ensligh - Vinari Software\nEspañol - Vinari Software");
		
			mainInstance.set_transient_for(parent);
			mainInstance.set_modal(false);
		}

		public void run(){
			this.mainInstance.present();
		}
	}
}
