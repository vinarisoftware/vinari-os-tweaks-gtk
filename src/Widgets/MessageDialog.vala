namespace App.Widgets{
	public class MessageDialog{
		public MessageDialog(Gtk.Window? windowParent, Gtk.MessageType messageType, Gtk.ButtonsType buttonsType, string dialogMessage){
			Gtk.MessageDialog thisDialog=new Gtk.MessageDialog(windowParent, Gtk.DialogFlags.MODAL, messageType, buttonsType, dialogMessage);
			thisDialog.set_modal(true);
			thisDialog.present();
			thisDialog.response.connect (() => {
				thisDialog.destroy();
			});
		}
	}
}
