/*
	Copyright (C) 2015-2025. Vinari Software.
	All rights reserved.​


	Redistribution and use in source and binary forms, source code with or without
	modification, are permitted provided that the following conditions are met:

	1. Redistributions of source code must retain the above copyright
	notice, this list of conditions and the following disclaimer.

	2. Redistributions in binary form must reproduce the above copyright
	notice, this list of conditions and the following disclaimer in the
	documentation and/or other materials provided with the distribution.

	3. Neither the name of the copyright holder nor the names of its
	contributors may be used to endorse or promote products derived
	from this software without specific prior written permission.

	THIS SOFTWARE IS PROVIDED BY VINARI SOFTWARE "AS IS" AND ANY
	EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL VINARI SOFTWARE BE LIABLE FOR ANY
	DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
	LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


	VINARI SOFTWARE 2015-2025
*/

namespace UI {
	[GtkTemplate (ui="/org/vinarios/tweaks/UI/MainWindow.ui")]
	public class MainWindow : Gtk.ApplicationWindow {
		[GtkChild] private unowned Gtk.Box mainBox;
		private Gtk.Stack windowArea;
		private Gtk.StackSwitcher stackSwitcher;

		public MainWindow (Gtk.Application app) {
			Object (application: app);
		}
		construct{
			this.set_title("Vinari OS Tweaks");
			this.mainBox.set_orientation(Gtk.Orientation.VERTICAL);
			
			this.set_resizable(false);

			windowArea=new Gtk.Stack();
			windowArea.set_transition_type(Gtk.StackTransitionType.CROSSFADE);
			
			UI.Views.Colours coloursView = new UI.Views.Colours();
			UI.Views.Dock dockView = new UI.Views.Dock();
			UI.Views.Fonts fontsView = new UI.Views.Fonts();
			UI.Views.Misc miscView = new UI.Views.Misc();

			windowArea.add_titled(coloursView, "colours", _("Style & Colours"));
			windowArea.add_titled(dockView, "dock", _("Dock & Desktop"));
			windowArea.add_titled(fontsView, "fonts", _("Fonts & Window"));
			windowArea.add_titled(miscView, "misc", _("Misc. Settings"));


			stackSwitcher=new Gtk.StackSwitcher();
			stackSwitcher.set_stack(windowArea);
			
			mainBox.append(stackSwitcher);
			mainBox.append(windowArea);
		}
	}
}
