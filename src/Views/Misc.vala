namespace UI.Views {
	public class Misc : Gtk.Box{
		Gtk.Separator separatorONE;
		Gtk.Separator separatorTWO;

		Gtk.Label resetLabel;
		Gtk.Label extensionsLabel;
		Gtk.Label warningLabel;

		Gtk.Button resetGridButton;
		Gtk.Button resetGNOMEButton;
		Gtk.Button trayIconSettingsButton;
		Gtk.Button desktopIconSettingsButton;
		Gtk.Button dockSettingsButton;

		App.Widgets.MessageDialog miscMessage;
		
		public Misc(){
			this.set_orientation(Gtk.Orientation.VERTICAL);
            this.set_spacing(15);
			this.set_margin_start(12);
			this.set_margin_end(7);
			this.set_margin_bottom(12);
		}
		
		construct {
			separatorONE=new Gtk.Separator(Gtk.Orientation.HORIZONTAL);
			separatorONE.set_margin_bottom(15);
			separatorTWO=new Gtk.Separator(Gtk.Orientation.HORIZONTAL);
			separatorTWO.set_margin_bottom(8);

			resetLabel=new Gtk.Label("");
			resetLabel.set_markup(_("<span weight=\"bold\">Reset parts of the desktop</span>"));
			extensionsLabel=new Gtk.Label("");
			extensionsLabel.set_markup(_("<span weight=\"bold\">Desktop component's Advanced settings</span>"));
			
			warningLabel=new Gtk.Label(_("The changed made in this section will afect the current user only.\nA reboot will be required to fully apply the changes."));

			// Reset application grid button setup
			Gtk.Image resetGridImage=new Gtk.Image.from_icon_name("grid-rectangular");
			Gtk.Label resetGridLabel=new Gtk.Label(_("Reset application grid to alphabetical order"));
			Gtk.Box resetGridBox=new Gtk.Box(Gtk.Orientation.HORIZONTAL, 10);
			resetGridBox.set_halign(CENTER);

			resetGridBox.append(resetGridImage);
			resetGridBox.append(resetGridLabel);

			resetGridButton=new Gtk.Button();
			resetGridButton.set_child(resetGridBox);

			// Reset GNOME button setup
			Gtk.Image resetGnomeImage=new Gtk.Image.from_icon_name("dconf-editor");
			Gtk.Label resetGnomeLabel=new Gtk.Label(_("Reset Vinari OS to out of the box settings"));
			Gtk.Box resetGnomeBox=new Gtk.Box(Gtk.Orientation.HORIZONTAL, 10);
			resetGnomeBox.set_halign(CENTER);

			resetGnomeBox.append(resetGnomeImage);
			resetGnomeBox.append(resetGnomeLabel);

			resetGNOMEButton=new Gtk.Button();
			resetGNOMEButton.set_child(resetGnomeBox);

			// Tray icon settings button setup
			Gtk.Image trayIconSettingsImage=new Gtk.Image.from_icon_name("icons");
			Gtk.Label trayIconsSettingsLabel=new Gtk.Label(_("Advanced tray icons settings"));
			Gtk.Box trayIconsSettingsBox=new Gtk.Box(Gtk.Orientation.HORIZONTAL, 10);
			trayIconsSettingsBox.set_halign(CENTER);

			trayIconsSettingsBox.append(trayIconSettingsImage);
			trayIconsSettingsBox.append(trayIconsSettingsLabel);

			trayIconSettingsButton=new Gtk.Button();
			trayIconSettingsButton.set_child(trayIconsSettingsBox);

			// Desktop icons settings button setup
			Gtk.Image desktopIconSettingsImage=new Gtk.Image.from_icon_name("desktop-profiler");
			Gtk.Label desktopIconSettingsLabel=new Gtk.Label(_("Advanced desktop icons settings"));
			Gtk.Box desktopIconSettingsBox=new Gtk.Box(Gtk.Orientation.HORIZONTAL, 10);
			desktopIconSettingsBox.set_halign(CENTER);

			desktopIconSettingsBox.append(desktopIconSettingsImage);
			desktopIconSettingsBox.append(desktopIconSettingsLabel);

			desktopIconSettingsButton=new Gtk.Button();
			desktopIconSettingsButton.set_child(desktopIconSettingsBox);

			// Dock settings button setup
			Gtk.Image dockSettingsImage=new Gtk.Image.from_icon_name("dock");
			Gtk.Label dockSettingsLabel=new Gtk.Label(_("Advanced dock settings"));
			Gtk.Box dockSettingsBox=new Gtk.Box(Gtk.Orientation.HORIZONTAL, 10);
			dockSettingsBox.set_halign(CENTER);

			dockSettingsBox.append(dockSettingsImage);
			dockSettingsBox.append(dockSettingsLabel);

			dockSettingsButton=new Gtk.Button();
			dockSettingsButton.set_child(dockSettingsBox);

			// Setup of click events for the buttons
			dockSettingsButton.clicked.connect(dockSettingsButtonAction);
			trayIconSettingsButton.clicked.connect(trayIconSettingsButtonAction);
			desktopIconSettingsButton.clicked.connect(desktopIconSettingsButtonAction);
			resetGNOMEButton.clicked.connect(resetGNOMEButtonAction);
			resetGridButton.clicked.connect(resetGridButtonAction);

			// Adding the widgets to the BOX
			this.append(resetLabel);
			this.append(resetGridButton);
			this.append(resetGNOMEButton);
			this.append(separatorONE);
			this.append(extensionsLabel);
			this.append(trayIconSettingsButton);
			this.append(desktopIconSettingsButton);
			this.append(dockSettingsButton);
			this.append(separatorTWO);
			this.append(warningLabel);
		}

		private void resetGridButtonAction(){
			Gtk.MessageDialog gridReset=new Gtk.MessageDialog(null, Gtk.DialogFlags.MODAL, QUESTION, YES_NO, _("Are you sure you want to sort the application's grid in alphabetical order?"));
			gridReset.set_modal(true);
			gridReset.present();
			gridReset.response.connect ((response) => {
				if(response == Gtk.ResponseType.YES){
					try{
						GLib.Process.spawn_command_line_async("gsettings set org.gnome.shell app-picker-layout \"[]\"");
						message("Application grid has been alphabetically ordered.");
					}catch(SpawnError e){
						miscMessage=new App.Widgets.MessageDialog(null, ERROR, OK, _("An unexpected error has occurred:\n")+ e.message);
					}
				}

				gridReset.destroy();
				return;
			});
		}

		private void resetGNOMEButtonAction(){
			Gtk.MessageDialog fullReset=new Gtk.MessageDialog(null, Gtk.DialogFlags.MODAL, QUESTION, YES_NO, _("Are you sure you want to reset Vinari OS to the default configuration?"));
			fullReset.set_modal(true);
			fullReset.present();
			fullReset.response.connect ((response) => {
				if(response == Gtk.ResponseType.YES){
					try{
						GLib.Process.spawn_command_line_async("dconf reset -f /");
						message("Dconf settings have been reset.");
					}catch(SpawnError e){
						miscMessage=new App.Widgets.MessageDialog(null, ERROR, OK, _("An unexpected error has occurred:\n")+ e.message);
					}
				}

				fullReset.destroy();
				return;
			});
		}

		private void dockSettingsButtonAction(){
			try{
				GLib.Process.spawn_command_line_async("gnome-extensions prefs dash-to-dock@micxgx.gmail.com");
			}catch(SpawnError e){
				miscMessage=new App.Widgets.MessageDialog(null, ERROR, OK, _("An unexpected error has occurred:\n")+ e.message);
			}
		}

		private void desktopIconSettingsButtonAction(){
			try{
				GLib.Process.spawn_command_line_async("gnome-extensions prefs ding@rastersoft.com");
			}catch(SpawnError e){
				miscMessage=new App.Widgets.MessageDialog(null, ERROR, OK, _("An unexpected error has occurred:\n")+ e.message);
			}
		}

		private void trayIconSettingsButtonAction(){
			try{
				GLib.Process.spawn_command_line_async("gnome-extensions prefs trayIconsReloaded@selfmade.pl");
			}catch(SpawnError e){
				miscMessage=new App.Widgets.MessageDialog(null, ERROR, OK, _("An unexpected error has occurred:\n")+ e.message);
			}
		}
	}
}
