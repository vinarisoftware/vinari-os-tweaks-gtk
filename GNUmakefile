#	Copyright (c) 2015 - 2025, Vinari Software
#	All rights reserved.
#
#	Redistribution and use in source and binary forms, with or without
#	modification, are permitted provided that the following conditions are met:
#
#	1. Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
#	2. Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
#	3. Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
#	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
#	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
#	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
#	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
#	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
#	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
#	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


BIN_NAME=org.vinarios.tweaks
BIN_DIR=./build/
DATA_DIR=./data/
SCHEMAS_DIR=/usr/share/glib-2.0/schemas/
I18N_DIR=./i18n/

.PHONY : config
config:
	@meson setup build

.PHONY : release
release:
	@meson setup build --buildtype=release

.PHONY : install-gschema
install-gschema:
	@chmod 644 $(DATA_DIR)$(BIN_NAME).gschema.xml
	@cp -v $(DATA_DIR)$(BIN_NAME).gschema.xml $(SCHEMAS_DIR)
	@glib-compile-schemas $(SCHEMAS_DIR)

.PHONY : clean
clean:
	@cd $(BIN_DIR) && ninja clean
	@rm -rf $(I18N_DIR)locale

.PHONY : update-i18n
update-i18n:
	@cd $(BIN_DIR) && ninja $(BIN_NAME)-update-po

.PHONY : i18n
i18n:
	@mkdir -pv $(I18N_DIR)locale/de/LC_MESSAGES/
	@mkdir -pv $(I18N_DIR)locale/es/LC_MESSAGES/
	@mkdir -pv $(I18N_DIR)locale/fr/LC_MESSAGES/
	@mkdir -pv $(I18N_DIR)locale/it/LC_MESSAGES/
	@mkdir -pv $(I18N_DIR)locale/pt/LC_MESSAGES/

	@msgfmt -o $(I18N_DIR)locale/de/LC_MESSAGES/$(BIN_NAME).mo $(I18N_DIR)de.po
	@msgfmt -o $(I18N_DIR)locale/es/LC_MESSAGES/$(BIN_NAME).mo $(I18N_DIR)es.po
	@msgfmt -o $(I18N_DIR)locale/fr/LC_MESSAGES/$(BIN_NAME).mo $(I18N_DIR)fr.po
	@msgfmt -o $(I18N_DIR)locale/it/LC_MESSAGES/$(BIN_NAME).mo $(I18N_DIR)it.po
	@msgfmt -o $(I18N_DIR)locale/pt/LC_MESSAGES/$(BIN_NAME).mo $(I18N_DIR)pt.po

	@chmod 755 $(I18N_DIR)locale/de/
	@chmod 755 $(I18N_DIR)locale/es/
	@chmod 755 $(I18N_DIR)locale/fr/
	@chmod 755 $(I18N_DIR)locale/it/
	@chmod 755 $(I18N_DIR)locale/pt/

	@chmod 755 $(I18N_DIR)locale/de/LC_MESSAGES/
	@chmod 755 $(I18N_DIR)locale/es/LC_MESSAGES/
	@chmod 755 $(I18N_DIR)locale/fr/LC_MESSAGES/
	@chmod 755 $(I18N_DIR)locale/it/LC_MESSAGES/
	@chmod 755 $(I18N_DIR)locale/pt/LC_MESSAGES/

	@chmod 644 $(I18N_DIR)locale/de/LC_MESSAGES/$(BIN_NAME).mo
	@chmod 644 $(I18N_DIR)locale/es/LC_MESSAGES/$(BIN_NAME).mo
	@chmod 644 $(I18N_DIR)locale/fr/LC_MESSAGES/$(BIN_NAME).mo
	@chmod 644 $(I18N_DIR)locale/it/LC_MESSAGES/$(BIN_NAME).mo
	@chmod 644 $(I18N_DIR)locale/pt/LC_MESSAGES/$(BIN_NAME).mo

.PHONY : uninstall
uninstall:
	@rm -rf /usr/share/vinarisoftware/tweaks/
	@rm -rf /usr/share/doc/vinari-os-tweaks-vinarisoftware/
	@rm -rf /usr/share/applications/$(BIN_NAME).desktop
	@rm -rf /usr/bin/$(BIN_NAME)
	@rm -rf /usr/share/glib-2.0/schemas/$(BIN_NAME).gschema.xml

	@rm -rf /usr/share/locale/de/LC_MESSAGES/$(BIN_NAME).mo
	@rm -rf /usr/share/locale/es/LC_MESSAGES/$(BIN_NAME).mo
	@rm -rf /usr/share/locale/fr/LC_MESSAGES/$(BIN_NAME).mo
	@rm -rf /usr/share/locale/it/LC_MESSAGES/$(BIN_NAME).mo
	@rm -rf /usr/share/locale/pt/LC_MESSAGES/$(BIN_NAME).mo

	@glib-compile-schemas /usr/share/glib-2.0/schemas/

	@rm -rf /usr/share/icons/hicolor/32x32/apps/$(BIN_NAME).png
	@rm -rf /usr/share/icons/hicolor/32x32@2/apps/$(BIN_NAME).png
	@rm -rf /usr/share/icons/hicolor/48x48/apps/$(BIN_NAME).png
	@rm -rf /usr/share/icons/hicolor/48x48@2/apps/$(BIN_NAME).png
	@rm -rf /usr/share/icons/hicolor/64x64/apps/$(BIN_NAME).png
	@rm -rf /usr/share/icons/hicolor/64x64@2/apps/$(BIN_NAME).png
	@rm -rf /usr/share/icons/hicolor/128x128/apps/$(BIN_NAME).png
	@rm -rf /usr/share/icons/hicolor/128x128@2/apps/$(BIN_NAME).png
	@rm -rf /usr/share/icons/hicolor/256x256/apps/$(BIN_NAME).png
	@rm -rf /usr/share/icons/hicolor/512x512/apps/$(BIN_NAME).png
	@rm -rf /usr/share/icons/hicolor/512x512@2/apps/$(BIN_NAME).png

.PHONY : install
install:
	@chmod +x $(BIN_DIR)src/$(BIN_NAME)
	@strip $(BIN_DIR)src/$(BIN_NAME)

	@mkdir -p /usr/share/vinarisoftware/tweaks/
	@mkdir -p /usr/share/doc/vinari-os-tweaks-vinarisoftware/
	@mkdir -p /usr/share/icons/hicolor/

	@mkdir -p /usr/share/icons/hicolor/32x32/apps/
	@mkdir -p /usr/share/icons/hicolor/32x32@2/apps/
	@mkdir -p /usr/share/icons/hicolor/48x48/apps/
	@mkdir -p /usr/share/icons/hicolor/48x48@2/apps/
	@mkdir -p /usr/share/icons/hicolor/64x64/apps/
	@mkdir -p /usr/share/icons/hicolor/64x64@2/apps/
	@mkdir -p /usr/share/icons/hicolor/128x128/apps/
	@mkdir -p /usr/share/icons/hicolor/128x128@2/apps/
	@mkdir -p /usr/share/icons/hicolor/256x256/apps/
	@mkdir -p /usr/share/icons/hicolor/512x512/apps/
	@mkdir -p /usr/share/icons/hicolor/512x512@2/apps/

	@cp -vr Assets/ /usr/share/vinarisoftware/tweaks/
	@cp -v LICENSE /usr/share/doc/vinari-os-tweaks-vinarisoftware/

	@cp -v $(DATA_DIR)$(BIN_NAME).desktop /usr/share/applications/
	@cp -v $(BIN_DIR)src/$(BIN_NAME) /usr/bin/

	@cp -v $(DATA_DIR)icons/32.png /usr/share/icons/hicolor/32x32/apps/$(BIN_NAME).png
	@cp -v $(DATA_DIR)icons/48.png /usr/share/icons/hicolor/48x48/apps/$(BIN_NAME).png
	@cp -v $(DATA_DIR)icons/64.png /usr/share/icons/hicolor/64x64/apps/$(BIN_NAME).png
	@cp -v $(DATA_DIR)icons/128.png /usr/share/icons/hicolor/128x128/apps/$(BIN_NAME).png
	@cp -v $(DATA_DIR)icons/256.png /usr/share/icons/hicolor/256x256/apps/$(BIN_NAME).png
	@cp -v $(DATA_DIR)icons/512.png /usr/share/icons/hicolor/512x512/apps/$(BIN_NAME).png

	@ln -s /usr/share/icons/hicolor/32x32/apps/$(BIN_NAME).png /usr/share/icons/hicolor/32x32@2/apps/$(BIN_NAME).png
	@ln -s /usr/share/icons/hicolor/48x48/apps/$(BIN_NAME).png /usr/share/icons/hicolor/48x48@2/apps/$(BIN_NAME).png
	@ln -s /usr/share/icons/hicolor/64x64/apps/$(BIN_NAME).png /usr/share/icons/hicolor/64x64@2/apps/$(BIN_NAME).png
	@ln -s /usr/share/icons/hicolor/128x128/apps/$(BIN_NAME).png /usr/share/icons/hicolor/128x128@2/apps/$(BIN_NAME).png
	@ln -s /usr/share/icons/hicolor/512x512/apps/$(BIN_NAME).png /usr/share/icons/hicolor/512x512@2/apps/$(BIN_NAME).png

	@mkdir -pv $(I18N_DIR)locale/de/LC_MESSAGES/
	@mkdir -pv $(I18N_DIR)locale/es/LC_MESSAGES/
	@mkdir -pv $(I18N_DIR)locale/fr/LC_MESSAGES/
	@mkdir -pv $(I18N_DIR)locale/it/LC_MESSAGES/
	@mkdir -pv $(I18N_DIR)locale/pt/LC_MESSAGES/

	@msgfmt -o $(I18N_DIR)locale/de/LC_MESSAGES/$(BIN_NAME).mo $(I18N_DIR)de.po
	@msgfmt -o $(I18N_DIR)locale/es/LC_MESSAGES/$(BIN_NAME).mo $(I18N_DIR)es.po
	@msgfmt -o $(I18N_DIR)locale/fr/LC_MESSAGES/$(BIN_NAME).mo $(I18N_DIR)fr.po
	@msgfmt -o $(I18N_DIR)locale/it/LC_MESSAGES/$(BIN_NAME).mo $(I18N_DIR)it.po
	@msgfmt -o $(I18N_DIR)locale/pt/LC_MESSAGES/$(BIN_NAME).mo $(I18N_DIR)pt.po

	@chmod 755 $(I18N_DIR)locale/de/
	@chmod 755 $(I18N_DIR)locale/es/
	@chmod 755 $(I18N_DIR)locale/fr/
	@chmod 755 $(I18N_DIR)locale/it/
	@chmod 755 $(I18N_DIR)locale/pt/

	@chmod 755 $(I18N_DIR)locale/de/LC_MESSAGES/
	@chmod 755 $(I18N_DIR)locale/es/LC_MESSAGES/
	@chmod 755 $(I18N_DIR)locale/fr/LC_MESSAGES/
	@chmod 755 $(I18N_DIR)locale/it/LC_MESSAGES/
	@chmod 755 $(I18N_DIR)locale/pt/LC_MESSAGES/

	@chmod 644 $(I18N_DIR)locale/de/LC_MESSAGES/$(BIN_NAME).mo
	@chmod 644 $(I18N_DIR)locale/es/LC_MESSAGES/$(BIN_NAME).mo
	@chmod 644 $(I18N_DIR)locale/fr/LC_MESSAGES/$(BIN_NAME).mo
	@chmod 644 $(I18N_DIR)locale/it/LC_MESSAGES/$(BIN_NAME).mo
	@chmod 644 $(I18N_DIR)locale/pt/LC_MESSAGES/$(BIN_NAME).mo

	@cp -v $(I18N_DIR)locale/de/LC_MESSAGES/$(BIN_NAME).mo /usr/share/locale/de/LC_MESSAGES/
	@cp -v $(I18N_DIR)locale/es/LC_MESSAGES/$(BIN_NAME).mo /usr/share/locale/es/LC_MESSAGES/
	@cp -v $(I18N_DIR)locale/fr/LC_MESSAGES/$(BIN_NAME).mo /usr/share/locale/fr/LC_MESSAGES/
	@cp -v $(I18N_DIR)locale/it/LC_MESSAGES/$(BIN_NAME).mo /usr/share/locale/it/LC_MESSAGES/
	@cp -v $(I18N_DIR)locale/pt/LC_MESSAGES/$(BIN_NAME).mo /usr/share/locale/pt/LC_MESSAGES/

	@chmod 644 $(DATA_DIR)$(BIN_NAME).gschema.xml
	@cp -v $(DATA_DIR)$(BIN_NAME).gschema.xml $(SCHEMAS_DIR)
	@glib-compile-schemas $(SCHEMAS_DIR)
